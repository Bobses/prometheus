Config files for Prometheus and Alertmanager.

Don't forget to check the validity of all YAML files!


# prometheus

Check the **prometheus.yml** with the following command:

**promtool check config prometheus.yml**<br/>
*Checking prometheus.yml*<br/>
  *SUCCESS: 1 rule files found*<br/>

*Checking /etc/prometheus/alert.rules*<br/>
  *SUCCESS: 5 rules found*<br/>



Check the **alert.rules** file with the following command:

**promtool check rules alert.rules**<br/>
*Checking alert.rules*<br/>
  *SUCCESS: 5 rules found*<br/>
  
  
# alertmanager
  
 Add the following line in the **alertmanager** service file (usually here - /usr/lib/systemd/system/alertmanager.service):
 
 ```
 [...]
 --cluster.listen-address= \
 [...]
 ```
 
 This line is necessary to disable the clustering mode which come by default with alertmanager. Without this line, the alertmanager service doesn't start at boot, even if the service is enabled.